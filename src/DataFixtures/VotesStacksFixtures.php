<?php

namespace App\DataFixtures;

use App\Entity\Owner;
use App\Entity\StackOfVotes;
use App\Entity\Vote;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class VotesStacksFixtures extends Fixture implements DependentFixtureInterface {
	public function getDependencies() {
		return [
			AppPollFixtures::class,
		];
	}

	public function load( ObjectManager $manager ) {

		$people1 = new Owner();
		$people1->setEmail( 'tktest_nikolas_edison@tktest.com' )
		        ->setPseudo( 'Nikolas Edison' );
		$people2 = new Owner();
		$people2->setEmail( 'wulfila@tktest.com' )
		        ->setPseudo( 'Wulfila' );
		$people3 = new Owner();
		$people3->setEmail( 'billie_jean@tktest.com' )
		        ->setPseudo( 'Billie Jean' );

		// "citron ou orange"
		// add vote stacks on "citron ou orange"
		$pollCitronOrange = $this->getReference( AppPollFixtures::POLL_FIXTURE_ONE );
		$stack1           = new StackOfVotes();
		$stack1
			->setPoll( $pollCitronOrange )
			->setOwner( $people1 );
		$voteA = new Vote();
		$voteA
			->setPoll( $pollCitronOrange )
			->setStacksOfVotes( $stack1 )
			->setValue( "yes" )
			->setChoice( $pollCitronOrange->getChoices()[ 0 ] );
		$voteB = new Vote();
		$voteB
			->setPoll( $pollCitronOrange )
			->setStacksOfVotes( $stack1 )
			->setValue( "maybe" )
			->setChoice( $pollCitronOrange->getChoices()[ 1 ] );
		$pollCitronOrange->addStackOfVote( $stack1 );
		$manager->persist( $pollCitronOrange );
		$manager->persist( $people1 );
		$manager->persist( $stack1 );

		$stack2 = new StackOfVotes();
		$stack2
			->setPoll( $pollCitronOrange )
			->setOwner( $people2 );
		$voteA = new Vote();
		$voteA
			->setPoll( $pollCitronOrange )
			->setStacksOfVotes( $stack2 )
			->setValue( "no" )
			->setChoice( $pollCitronOrange->getChoices()[ 0 ] );
		$voteB = new Vote();
		$voteB
			->setPoll( $pollCitronOrange )
			->setStacksOfVotes( $stack2 )
			->setValue( "yes" )
			->setChoice( $pollCitronOrange->getChoices()[ 1 ] );
		$pollCitronOrange->addStackOfVote( $stack2 );
		$manager->persist( $pollCitronOrange );
		$manager->persist( $stack2 );
		$manager->persist( $people2 );


//		$stack1 = new StackOfVotes();
//		$stack1
//			->setPoll( $pollCitronOrange )
//			->setOwner( $people1 );
//		$voteA = new Vote();
//		$voteA
//			->setPoll( $pollCitronOrange )
//			->setStacksOfVotes( $stack1 )
//			->setValue( "maybe" )
//			->setChoice( $pollCitronOrange->getChoices()[ 2 ] );
//		$voteB = new Vote();
//		$voteB
//			->setPoll( $pollCitronOrange )
//			->setStacksOfVotes( $stack1 )
//			->setValue( "maybe" )
//			->setChoice( $pollCitronOrange->getChoices()[ 4 ] );
//		$pollCitronOrange->addStackOfVote( $stack1 );
//		$manager->persist( $pollCitronOrange );
//		$manager->persist( $people1 );
//		$manager->persist( $stack1 );
//
//
//		$stack2 = new StackOfVotes();
//		$stack2
//			->setPoll( $pollCitronOrange )
//			->setOwner( $people2 );
//		$voteA = new Vote();
//		$voteA
//			->setPoll( $pollCitronOrange )
//			->setStacksOfVotes( $stack2 )
//			->setValue( "maybe" )
//			->setChoice( $pollCitronOrange->getChoices()[ 3 ] );
//		$voteB = new Vote();
//		$voteB
//			->setPoll( $pollCitronOrange )
//			->setStacksOfVotes( $stack2 )
//			->setValue( "yes" )
//			->setChoice( $pollCitronOrange->getChoices()[ 5 ] );
//		$pollCitronOrange->addStackOfVote( $stack2 );
//		$manager->persist( $pollCitronOrange );
//		$manager->persist( $people2 );
//		$manager->persist( $stack2 );
//
//
//		$stack3 = new StackOfVotes();
//		$stack3
//			->setPoll( $pollCitronOrange )
//			->setOwner( $people3 );
//		$voteA = new Vote();
//		$voteA
//			->setPoll( $pollCitronOrange )
//			->setStacksOfVotes( $stack3 )
//			->setValue( "yes" )
//			->setChoice( $pollCitronOrange->getChoices()[ 1 ] );
//		$voteB = new Vote();
//		$voteB
//			->setPoll( $pollCitronOrange )
//			->setStacksOfVotes( $stack3 )
//			->setValue( "yes" )
//			->setChoice( $pollCitronOrange->getChoices()[ 3 ] );
//		$pollCitronOrange->addStackOfVote( $stack3 );
//		$manager->persist( $pollCitronOrange );
//		$manager->persist( $people3 );
//		$manager->persist( $stack3 );


		// comment on "démo sondage de texte avec deux commentaires"
//		$poll = $emPoll->find( 2 );
//
//
//		// comment on "c'est pour aujourdhui ou pour demain"
//		$poll = $emPoll->find( 3 );
//
//
//		// comment on "dessin animé préféré"
//		$poll = $emPoll->find( 4 );

		$manager->flush();
	}
}
