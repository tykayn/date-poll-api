<?php

namespace App\DataFixtures;

use App\Entity\Comment;
use App\Entity\Owner;
use App\Entity\Poll;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class CommentFixtures extends Fixture implements DependentFixtureInterface {
	public function getDependencies() {
		return [
			AppPollFixtures::class,
		];
	}

	public function load( ObjectManager $manager ) {
		$emPoll = $manager->getRepository( Poll::class );

		$commenterMan = new Owner();
		$commenterMan->setEmail( 'tktest_commentateur_other@tktest.com' )
		             ->setPseudo( 'Bill Murray' );
		$commenterMan2 = new Owner();
		$commenterMan2->setEmail( 'wulfila@tktest.com' )
		              ->setPseudo( 'Wulfila' );

		// comment on "citron ou orange"
		$pollCitronOrange = $this->getReference( AppPollFixtures::POLL_FIXTURE_ONE );
		$comment          = new Comment();
		$comment->setOwner( $commenterMan )
		        ->setPseudo( 'the indécis people' )
		        ->setText( 'quelle indécision wololo! finalement citron. heu non orange. AAAAh!' );
		$pollCitronOrange->addComment( $comment );
		$manager->persist( $comment );
		$manager->persist( $pollCitronOrange );
		$manager->persist( $commenterMan );
		$manager->flush();

		// comment on "démo sondage de texte avec deux commentaires"
//		$poll    = $emPoll->find( 2 );
//		$comment = new Comment();
//		$comment->setOwner( $commenterMan2 )
//		        ->setPseudo( 'The Hayroule king' )
//		        ->setText( 'il est écrit Squalala.' );
//		$pollCitronOrange->addComment( $comment );
//		$manager->persist( $comment );
//		$manager->persist( $poll );
//		$manager->persist( $commenterMan2 );
//		$manager->flush();
//
//		$comment = new Comment();
//		$comment->setOwner( $commenterMan )
//		        ->setPseudo( 'The Hayroule king' )
//		        ->setText( "Zelda. Orange." );
//		$pollCitronOrange->addComment( $comment );
//		$manager->persist( $comment );
//		$manager->persist( $poll );
//		$manager->persist( $commenterMan );
//		$manager->flush();
//
//
//		// comment on "c'est pour aujourdhui ou pour demain"
//		$poll    = $emPoll->find( 3 );
//		$comment = new Comment();
//		$comment->setOwner( $commenterMan )
//		        ->setPseudo( 'The Hayroule king' )
//		        ->setText( "va pour demain" );
//		$pollCitronOrange->addComment( $comment );
//		$manager->persist( $comment );
//		$manager->persist( $poll );
//		$manager->persist( $commenterMan );
//		$manager->flush();
//
//		// comment on "dessin animé préféré"
//		$poll    = $emPoll->find( 4 );
//		$comment = new Comment();
//		$comment->setOwner( $commenterMan2 )
//		        ->setPseudo( 'The Hayroule king' )
//		        ->setText( "Ceci est un commentaire de fixture créé avec le CipherBliss poweur." );
//		$pollCitronOrange->addComment( $comment );
//		$manager->persist( $comment );
//		$manager->persist( $poll );
//		$manager->persist( $commenterMan2 );

		$manager->flush();
	}
}
