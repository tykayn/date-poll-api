<?php

namespace App\Controller;

use App\Entity\Owner;
use App\Entity\Poll;
use JMS\Serializer\Type\Exception\Exception;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\Exception\HttpTransportException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;

/**
 * sending emails controller
 *
 * Class EmailsController
 * @package App\Controller
 */
class EmailsController extends AbstractController {

	private $mail_service;

	public function __construct( Swift_Mailer $mailer ) {
		$this->mail_service = $mailer;
	}

	/**
	 * send created polls to an owner
	 *
	 * @param Owner $owner
	 *
	 * @return int|void
	 * @throws Exception
	 * @throws TransportExceptionInterface
	 */
	public function sendOwnerPollsAction( Owner $owner ) {

		$config = [
			'owner'          => $owner,
			'title'          => $this->getParameter( 'WEBSITE_NAME' ) . ' | Mes sondages',
			'email_template' => 'emails/owner-list.html.twig',
		];
		$this->sendMailWithVars( $config );

		return 1;
	}

	/**
	 * generic way to send email with html template
	 *
	 * @param $config
	 *
	 * @throws TransportExceptionInterface
	 */
	public function sendMailWithVars( $config) {

		if ( ! isset( $config[ 'poll' ] ) ) {
			$config[ 'poll' ] = new Poll();
		}
		if ( $config[ 'email_template' ] === 'owner_list' ) {

			// refuse to send all its poll list to an owner by email if it were asked less than 10 seconds ago

			$requested = $config[ 'owner' ]->getRequestedPollsDate(); //from database

			$today_time  = strtotime( date( "Y-m-d" ) );
			$expire_time = strtotime( $requested );
			if ( $expire_time - $today_time < 10 ) {
				throw new \HttpException( "you asked for this email less than 10 seconds ago. wait a little.", 403 );
			}

			$config[ 'owner' ]->setRequestedPollsDate( new \DateTime() );
		}
		$em = $this->getDoctrine()->getManager();
		$em->persist( $config[ 'owner' ] );
		$em->flush();


		$emailChoicesTemplates = [
			'creation_poll'       => 'creation-mail.html.twig',
			'edit_poll'           => 'modification-notification-mail.html.twig',
			'creation_poll_admin' => 'author-mail.html.twig',
			'owner_list'          => 'owner-list.html.twig',
			'expiration'          => 'expiration-mail.html.twig',
			'creation_comment'    => 'comment-notification.html.twig',
			'creation_vote'       => 'vote-notification.html.twig',
		];
		$emailChoicesTitles    = [
			'creation_poll'       => 'Framadate | Création de sondage - lien public - ' . $config[ 'poll' ]->getTitle(),
			'edit_poll'           => 'Framadate | Modification de sondage - ' . $config[ 'poll' ]->getTitle(),
			'creation_poll_admin' => 'Framadate | Création de sondage - lien admin - ',
			'owner_list'          => 'Framadate | Vos sondages créés',
			'expiration'          => 'Framadate | Notice d\'expiration du sondage ' . $config[ 'poll' ]->getTitle(),
			'creation_comment'    => 'Framadate | Commentaire de "' . $config[ 'owner' ]->getPseudo() . '" - sondage ' . $config[ 'poll' ]->getTitle(),
			'creation_vote'       => 'Framadate | Vote de "' . $config[ 'owner' ]->getPseudo() . '" - sondage ' . $config[ 'poll' ]->getTitle(),
		];
		// Create a message
		$htmlbody = $this->renderView(
			$config[ 'email_template' ],
			$config
		);
		$message  = ( new Swift_Message( $config[ 'title' ] ) )
			->setContentType( "text/html" )
			->setCharset( 'UTF-8' )
			->setFrom( [ 'ne-pas-repondre@framadate-api.cipherbliss.com' ] )
			->setTo( [ $config[ 'owner' ]->getEmail() ] )
			->setBody( $htmlbody, 'text/html' );


// Send the message
		try {
			$numSent       = $this->mail_service->send( $message );

			$this->numSent = $numSent;
		} catch ( TransportExceptionInterface $err ) {
			// some error prevented the email sending; display an
			// error message or try to resend the message

			throw $err;
		}


	}

	/**
	 * @param Owner $foundOwner
	 * @param Poll|null $poll
	 *
	 * @throws TransportExceptionInterface
	 */
	public function sendCreationMailAction( Owner $foundOwner, Poll $poll = null ) {

		// anti spam , limit to every minute TODO

		$config = [
			'owner'          => $foundOwner,
			'from'           => 'ne-pas-repondre@framadate-api.cipherbliss.com',
			'poll'           => $poll,
			'title'          => 'Création de sondage - ' . ( $poll ? $poll->getTitle() : $poll ),
			'email_template' => 'emails/creation-mail.html.twig',
		];

		return $this->sendMailWithVars( $config );
	}

	/**
	 * @param Owner $owner
	 * @param $comment
	 *
	 * @return int
	 * @throws TransportExceptionInterface
	 */
	public function sendCommentNotificationAction( Owner $owner, $comment ) {

		$config = [
			'owner'          => $owner,
			'comment'        => $comment,
			'poll'           => $comment->getPoll(),
			'title'          => 'Framadate | Commentaire de "' . $owner->getPseudo() . '" - sondage ' . $comment->getPoll()->getTitle(),
			'email_template' => 'emails/comment-notification.html.twig',
		];
		$this->sendMailWithVars( $config );

		return 1;
	}

	/**
	 * @param Owner $owner
	 * @param $stackOfVotes
	 *
	 * @return int
	 * @throws TransportExceptionInterface
	 */
	public function sendVoteNotificationAction( Owner $owner, $stackOfVotes ) {

		$config = [
			'owner'          => $owner,
			'comment'        => $stackOfVotes,
			'poll'           => $stackOfVotes->getPoll(),
			'title'          => 'Framadate | Vote de "' . $owner->getPseudo() . '" - sondage ' . $stackOfVotes->getPoll()->getTitle(),
			'email_template' => 'emails/vote-notification.html.twig',
		];
		$this->sendMailWithVars( $config );

		return 1;
	}
}
