<?php

namespace App\GraphQL\Resolver;

use Doctrine\ORM\EntityManager;
use Overblog\GraphQLBundle\Definition\Argument;
use Overblog\GraphQLBundle\Definition\Resolver\AliasedInterface;
use Overblog\GraphQLBundle\Definition\Resolver\ResolverInterface;

class ApartmentResolver implements ResolverInterface {

	private $em;

	public function __construct( EntityManager $em ) {
		$this->em = $em;
	}

	public function resolve( Argument $args ) {
		$apartment = $this->em->getRepository( 'App:Apartment' )->find( $args[ 'id' ] );

		return $apartment;
	}

	public function getAliases() {
		return [
			'resolve',
			'Apartment',
		];
	}
}
