<?php

namespace App\Entity;

use App\Traits\TimeStampableTrait;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\VoteRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Vote {

	use TimeStampableTrait;

	/**
	 * for a text kind of choice: could be "yes" "no" "maybe" and empty.
	 * for a date kind, the choice linked is equivalent to the value selected
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	public $value;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Choice", inversedBy="votes", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false)
	 * @Serializer\Type("App\Entity\choice")
	 * @Serializer\Expose()
	 */
	public $choice;
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 * @Serializer\Type("integer")
	 * @Serializer\Expose()
	 */
	private $id;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="votes", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $poll;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\StackOfVotes", inversedBy="votes", cascade={"persist"})
	 * @ORM\JoinColumn(nullable=false)
	 * @Serializer\Type("App\Entity\StackOfVotes")
	 */
	private $stacksOfVotes;

	public function __construct() {
		$this->setCreatedAt( new DateTime() );
	}

	public function display() {
		$value = $this->getValue();
		if ( ! $value ) {
			return null;
		} else {

			return [
				'id'        => $this->getId(),
				'value'     => $this->getValue(),
				'choice_id' => $this->getChoice()->getId(),
				'text'      => $this->getChoice()->getName(),
			];
		}
	}

	public function getValue(): ?string {
		return $this->value;
	}

	public function setValue( ?string $value ): self {
		$this->value = $value;

		return $this;
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getChoice(): ?Choice {
		return $this->choice;
	}

	public function setChoice( ?Choice $choice ): self {
		$this->choice = $choice;

		return $this;
	}

	public function getPoll(): ?Poll {
		return $this->poll;
	}

	public function setPoll( ?Poll $poll ): self {
		$this->poll = $poll;
		if ( $poll ) {
			$poll->addVote( $this );
		}

		return $this;
	}


	public function getStacksOfVotes(): ?StackOfVotes {
		return $this->stacksOfVotes;
	}

	public function setStacksOfVotes( ?StackOfVotes $stacksOfVotes ): self {
		$this->stacksOfVotes = $stacksOfVotes;

		return $this;
	}
}
