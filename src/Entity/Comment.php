<?php

namespace App\Entity;

use App\Traits\TimeStampableTrait;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CommentRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Comment {

	use TimeStampableTrait;

	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Owner", inversedBy="comments")
	 * @Serializer\Type("App\Entity\Owner")
	 * @Serializer\Expose()
	 */
	private $owner;

	/**
	 * @ORM\Column(type="string")
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	private $pseudo;

	/**
	 * @ORM\Column(type="text")
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	private $text;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="comments")
	 */
	private $poll;

	function __construct() {
		$this->setCreatedAt( new DateTime() );
	}

	public function setCreatedAt( DateTimeInterface $createdAt ): self {
		$this->createdAt = $createdAt;

		return $this;
	}

	public function getCreatedAt(): ?DateTimeInterface {
		return $this->createdAt;
	}

	function display() {
		return [
			'id'         => $this->getId(),
			'text'       => $this->getText(),
			'pseudo'     => $this->getOwner()->getPseudo(),
			'created_at' => $this->getCreatedAtAsString(),
		];
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function getText(): ?string {
		return $this->text;
	}

	public function setText( string $text ): self {
		$this->text = $text;

		return $this;
	}

	public function getOwner(): ?Owner {
		return $this->owner;
	}

	public function setOwner( ?Owner $owner ): self {
		$this->owner = $owner;

		return $this;
	}

	public function getPoll(): ?Poll {
		return $this->poll;
	}

	public function setPoll( ?Poll $poll ): self {
		$this->poll = $poll;

		return $this;
	}

	public function getPseudo(): ?string {
		return $this->pseudo;
	}

	public function setPseudo( string $pseudo ): self {
		$this->pseudo = $pseudo;

		return $this;
	}
}
