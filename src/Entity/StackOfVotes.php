<?php

namespace App\Entity;

use App\Traits\TimeStampableTrait;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * contains the votes for one answer to a poll
 * @ORM\Entity(repositoryClass="App\Repository\StackOfVotesRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Serializer\ExclusionPolicy("all")
 */
class StackOfVotes {

	use TimeStampableTrait;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Serializer\Type("string")
	 * @Serializer\Expose()
	 */
	public $pseudo;
	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Vote", mappedBy="stacksOfVotes",  orphanRemoval=true, cascade={"persist","remove"})
	 * @Serializer\Expose()
	 */
	public $votes;
	/**
	 * @ORM\Id()
	 * @ORM\GeneratedValue()
	 * @ORM\Column(type="integer")
	 */
	private $id;
	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Poll", inversedBy="stacksOfVotes", cascade={"persist"})
	 */
	private $poll;

	/**
	 * @ORM\ManyToOne(targetEntity="App\Entity\Owner", inversedBy="stackOfVotes", cascade={"persist"})
	 * @Serializer\Expose()
	 */
	private $owner;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	private $ip;

	public function __construct() {
		$this->setCreatedAt( new DateTime() );
		$this->votes = new ArrayCollection();
	}

	public function display() {


		// fill all votes by choices
		$choices      = $this->getPoll()->getChoices();
		$defaultVotes = [];
		foreach ( $choices as $choice ) {
			$defaultVotes[ $choice->getId() ] = [ "choice_id" => $choice->getId(), "value" => "" ];
		}

		$tab = [
			'id'         => $this->getId(),
			'pseudo'     => $this->getPseudo(),
			'created_at' => $this->getCreatedAtAsString(),
			'votes'      => $defaultVotes,
		];
		foreach ( $this->getVotes() as $vote ) {
			$tab[ 'votes' ][ $vote->getChoice()->getId() ] = $vote->display();
		}
		// flatten the votes array
		$flatVotes = [];
		foreach ( $tab[ 'votes' ] as $vote ) {
			$flatVotes[] = $vote;
		}
		$tab[ 'votes' ] = $flatVotes;

		$tab[ 'owner' ] = $this->getOwner()->display();

		return $tab;
	}

	public function displayForAdmin() {
		$tab            = $this->display();
		$tab[ 'owner' ] = $this->getOwner()->displayForAdmin();

		return $tab;
	}

	/**
	 * @return Collection|poll[]
	 */
	public function getVotes(): Collection {
		return $this->votes;
	}

	public function getPseudo(): ?string {
		return $this->pseudo;
	}

	public function setPseudo( ?string $pseudo ): self {
		$this->pseudo = $pseudo;

		return $this;
	}

	/**
	 * @ORM\PrePersist
	 */
	public function prePersist() {
		$this->setPseudo( $this->getOwner()->getPseudo() );
	}

	public function getOwner(): ?Owner {
		return $this->owner;
	}

	public function setOwner( ?Owner $owner ): self {
		$this->owner = $owner;

		return $this;
	}

	public function getId(): ?int {
		return $this->id;
	}

	public function addVote( Vote $vote ): self {
		if ( ! $this->votes->contains( $vote ) ) {
			$vote->setPoll( $this->getPoll() );

			$this->votes[] = $vote;
			$vote->setStacksOfVotes( $this );
		}

		return $this;
	}

	public function getPoll(): ?Poll {
		return $this->poll;
	}

	public function setPoll( ?Poll $poll ): self {
		$this->poll = $poll;

		return $this;
	}

	public function removeVote( Vote $vote ): self {
		if ( $this->votes->contains( $vote ) ) {
			$this->votes->removeElement( $vote );
			// set the owning side to null (unless already changed)
			if ( $vote->getStacksOfVotes() === $this ) {
				$vote->setStacksOfVotes( null );
			}
		}

		return $this;
	}

	public function getIp(): ?string {
		return $this->ip;
	}

	public function setIp( string $ip ): self {
		$this->ip = $ip;

		return $this;
	}

}
