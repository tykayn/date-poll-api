#Funky Framadate API
REST backend in symfony 5 for Funky framadate frontend.
https://framagit.org/framasoft/framadate/funky-framadate-front




------------------------------------ --------------- -------- ------ -------------------------------------------------- 
  Name                                 Method          Scheme   Host   Path                                              
 ------------------------------------ --------------- -------- ------ -------------------------------------------------- 
_preview_error                       ANY             ANY      ANY    /_error/{code}.{_format}                          
_wdt                                 ANY             ANY      ANY    /_wdt/{token}                                     
_profiler_home                       ANY             ANY      ANY    /_profiler/                                       
_profiler_search                     ANY             ANY      ANY    /_profiler/search                                 
_profiler_search_bar                 ANY             ANY      ANY    /_profiler/search_bar                             
_profiler_phpinfo                    ANY             ANY      ANY    /_profiler/phpinfo                                
_profiler_search_results             ANY             ANY      ANY    /_profiler/{token}/search/results                 
_profiler_open_file                  ANY             ANY      ANY    /_profiler/open                                   
_profiler                            ANY             ANY      ANY    /_profiler/{token}                                
_profiler_router                     ANY             ANY      ANY    /_profiler/{token}/router                         
_profiler_exception                  ANY             ANY      ANY    /_profiler/{token}/exception                      
_profiler_exception_css              ANY             ANY      ANY    /_profiler/{token}/exception.css                  
admin_homepage_get_default           GET             ANY      ANY    /admin/                                           
admin_homepage_clean_expired_polls   GET             ANY      ANY    /admin/polls/clean/{token}                        
admin_homepage_migrate_framadate     GET             ANY      ANY    /admin/polls/migrate                              
home_sweet_home                      GET             ANY      ANY    /                                                 
admin_homepage_migrate_from_v1       GET             ANY      ANY    /migration-from-v1/{unique_key}                   
user_homepageget_default             GET             ANY      ANY    /user/                                            
user_homepage_polls_send_by_email    GET             ANY      ANY    /user/{email}/polls/send-by-email                 
poll_index                           GET             ANY      ANY    /poll/                                            
poll_new                             POST            ANY      ANY    /poll/new                                         
poll_show                            GET             ANY      ANY    /poll/id/{id}                                     
poll_edit                            GET|POST        ANY      ANY    /poll/{id}/edit                                   
poll_delete                          DELETE          ANY      ANY    /poll/{id}                                        
api_get_poll_comment                 GET             ANY      ANY    /api/v1/comment/poll/{id}/comments                
api_new_comment                      POST            ANY      ANY    /api/v1/comment/poll/{id}/comment                 
api_poll_comments_delete             DELETE          ANY      ANY    /api/v1/comment/poll/{id}/comments                
api_essai                            GET             ANY      ANY    /api/v1/poll/essai                                
api_get_all_polls                    GET             ANY      ANY    /api/v1/poll/                                     
api_get_poll                         GET             ANY      ANY    /api/v1/poll/{customUrl}                          
api_get_owner_poll                   GET             ANY      ANY    /api/v1/poll/owner/{owner_email}/                 
api_get_protected_poll               GET             ANY      ANY    /api/v1/poll/{customUrl}/pass/{md5}               
api_get_admin_poll                   GET             ANY      ANY    /api/v1/polladmin/{admin_key}                     
api_update_poll                      PUT             ANY      ANY    /api/v1/poll/{customUrl}/update/{token}           
api_app_api_v1_poll_newpoll          POST            ANY      ANY    /api/v1/poll/                                     
api_app_api_v1_poll_newpoll.1        POST            ANY      ANY    /api/v1/poll_new_poll                             
api_test-mail-poll                   GET             ANY      ANY    /api/v1/poll/mail/test-mail-poll/{emailChoice}    
api_poll_delete                      DELETE          ANY      ANY    /api/v1/poll/{admin_key}                          
api_check_slug_is_unique             GET             ANY      ANY    /api/v1/poll/slug/{customUrl}                     
api_get_admin_config                 GET             ANY      ANY    /api/v1/poll/admin/{token}                        
api_clean_expired_polls              GET             ANY      ANY    /api/v1/poll/admin/clean_expired_polls/{token}    
api_new_vote_stack                   POST|OPTIONS    ANY      ANY    /api/v1/vote-stack/                               
api_update_vote_stack                PATCH|OPTIONS   ANY      ANY    /api/v1/vote-stack/{id}/token/{modifierToken}     
api_delete_vote_stack                DELETE          ANY      ANY    /api/v1/vote-stack/{id}/token/{modifierToken}     
api_poll_votes_delete                DELETE          ANY      ANY    /api/v1/vote-stack/poll/{id}/votes/{accessToken}  
app.swagger                          GET             ANY      ANY    /api/doc.json
 ------------------------------------ --------------- -------- ------ --------------------------------------------------

## TODO:
https://framagit.org/tykayn/date-poll-api/-/boards
* coordinate properties and paths to be more restful.

see [doc/examples.md](doc/examples.md)

***
### Requirements
You MUST have [php version 8+ (see installation doc)](doc/php-8-install.md) and [Composer](https://getcomposer.org/download/), and a database like Mysql, or postgresql, or mongodb, or any database that Doctrine can handle.
Setup access to database by creating a .env.local file, and fill the database user and password data.
you can check that the database connection works with the command:
```bash  
php bin/console doctrine:schema:validate
```

## Getting started
Follow this guide to setup.

# Development
install dependencies with Composer

configure env by creating local env file
```
cp .env .env.local
```
and edit the file variables to give access to database.

Launch dev server with the "symfony" cli tool:
```
symfony serve
```
there are examples of request to make it all work in the [doc/examples.md](doc/examples.md).

### Check prerequisites
```bash
composer check-platform-reqs
```

### install the vendors
```bash
composer install
```

#Funky Framadate API
Experimental REST backend in symfony 5 for Funky framadate frontend.
https://framagit.org/framasoft/framadate/funky-framadate-front

Follow this guide to setup.
* run `composer install`
configure env by creating local env file 
`cp .env .env.local` and edit the file variables to give access to database.

## TODO:
* coordinate properties and paths to be more restful.
  https://stackoverflow.blog/2020/03/02/best-practices-for-rest-api-design/
  
return stack of votes when we want to get the configuration of one poll (see [doc/examples.md](doc/examples.md))

***
### Requirements
You MUST have php version 8+ and [Composer](https://getcomposer.org/download/), and a database like Mysql, or postgresql, or mongodb, or any database that Doctrine can handle.
Setup access to database by creating a .env.local file, and fill the database user and password data.
you can check that the database connection works with the command:
```bash  
php bin/console doctrine:schema:validate
```

## Getting started
Have the right dependencies, a domain name, and run the install script.
### Dependencies
* PHP 8+
* symfony CLI tool
* composer, the PHP dependencies manager
* a database like Mysql
* a proper .env file
### run scripts
```bash
bash update.sh
symfony serve # to start the API on https://localhost:8000
```

# Development
install dependencies with Composer


there are examples of request to make it all work in the [doc/examples.md](doc/examples.md).

### Check prerequisites
```bash
composer check-platform-reqs
```

### install the vendors
```bash
composer install
```
### configure database access in .env.local file
This project can use any database system, we provide config for mysql.

If you need to setup mysql, a database and a user, read the [database_creation](doc/database_creation.md) doc. Before customizing your local environment config.
the .env.local file will not be versionned by git, so no risk to set the mysql credentials in here as long as file access are well done.

You can copy the model env file and customize it last line to access mysql.
```bash
cp doc/env-example .env.local
```
look for the line like this
> DATABASE_URL=mysql://framadate-admin:framadate-admin-password@127.0.0.1:5432/framadate-api

 and change its values for `framadate-admin` , `framadate-admin-password`, `framadate-api`.
 
### initiate the database with fixtures
```bash 
php bin/console doctrine:schema:drop --force
php bin/console doctrine:schema:create
php bin/console doctrine:fixtures:load --no-interaction --purge-with-truncate
```
### launch local server with
```bash
php bin/console server:run
```


# Production
set a virtual host on your server, configure CORS access to have the API to work.
configure database access in a .env.local file , replace variables
DATABASE_URL=mysql://database_user:db_user_password@127.0.0.1:3306/database_name
this file is not versionned and should stay like this.
## Setup the default funky frontend
Use the scripts to get the framadate funky frontend and update it
```bash 
bash front_update.sh
```
If you wish to develop things for the frontend, do not rebuild it each time, use a live server in the funky-framadate-front folder.

## cronjob to delete expired polls
you can open your crontabl in command line with :
```
crontab -e
```

add this line in your crontab to run the clearance of expired polls everyday at 0h00.
```
0 0 * * * wget http://MYWEBSITE/api/v1/poll/clean-polls 
```
Cronjob to send mails from the swiftmailer spool.
```
* * * * *	php /var/www/html/date-poll-api/bin/console swiftmailer:spool:send
```
you can disable the spooling, check the docs.


# About

made by B. Lemoine, aka Tykayn, for the framadate funky front end project, a polling libre software.
## contacts
* contact@cipherbliss.com
* https://mastodon.cipherbliss.com/@tykayn
* https://twitter.com/tykayn
* https://cipherbliss.com
