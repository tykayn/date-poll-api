# API example
after having setup your project and database, and having a local server running you can rest these commands
## get Swagger configuration

```
GET
http://127.0.0.1:8000/api/doc.json
```

## create a poll
```http request
POST http://127.0.0.1:8000/api/v1/poll/new
Content-Type:"application/json"

{
    "title": "un coup à boire",
    "description": "et on boit quoi?",
    "kind": "text",
    "custom_url": "this-is-sparta",
    "password":"hah",
    "owner": {
        "email": "contact@cipherbliss.com",
        "pseudo": "cipherbliss crée un sondage par postman"
    },
    "choices_to_create": [ "cappou" ,"thé", "café", "vodka"  ]
}

```

## get configuration of existing poll, protected with a password



```http request
GET http://127.0.0.1:8000/api/v1/poll/1
Content-Type:"application/json"
{
	"password_input": "there_is_my_pass"
}

```

## add a vote to an existing poll
```http request
POST http://127.0.0.1:8000/api/v1/poll/1/vote
Content-Type:"application/json"
{
    "pseudo": "tktest",
    "email": "testing_vote_people@tktest.com",
    "votes": [
        {
            "choice_id": 5,
            "value": "no"
        },
        {
            "choice_id": 6,
            "value": "maybe"
        },
        {
            "choice_id": 7,
            "value": "yes"
        }
    ]
}
```
