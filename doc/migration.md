# Migration depuis la version 1 de Framadate

Cela va importer vos anciens sondages dans les tables de la nouvelle version.

* Remplissez les accès en base de données concernant la version 1 de Framadate dans votre fichier [.env](../.env) à la racine de ce dossier.
* Choisissez une chaine unique dans votre fichier .env à la place de superCaliFragilistiExpialidousiousse
* Une fois votre application accessible via navigateur, rendez vous sur votre point d'accès web à l'addresse /migration-from-v1/superCaliFragilistiExpialidousiousse en changeant __superCaliFragilistiExpialidousiousse__ par votre variable unique.
* tadam, vous aurez un message indiquant si vous avez réussi la migration.


## Comparaison de schémas
pour information, voici les schémas des bases de données des deux versions:
### Ancien
![old](../public/assets/img/framadate_v1_schema.svg)
### Nouveau
![old](../public/assets/img/framadate_funky_schema.svg)
