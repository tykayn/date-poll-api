# Évolutions
Après avoir modifié le modèle de données dans les Entity, lancez la commande pour mettre à jour les getter et setters
```bash
php bin/console make:entity --regenerate
```

et pour mettre à jour le schéma en base de données
```bash
php bin/console doctrine:schema:update --force
```

# Ressources

les types de champ Doctrine: 
https://www.doctrine-project.org/projects/doctrine-orm/en/2.8/reference/basic-mapping.html#doctrine-mapping-types
