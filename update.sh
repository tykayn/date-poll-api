#!/bin/bash
# script d'update de framadate api, fait pour fonctionner avec le sous module git funky framadate


# bash colors
cecho() {
  local code="\033["
  case "$1" in
    black  | bk) color="${code}0;30m";;
    red    |  r) color="${code}1;31m";;
    green  |  g) color="${code}1;32m";;
    yellow |  y) color="${code}1;33m";;
    blue   |  b) color="${code}1;34m";;
    purple |  p) color="${code}1;35m";;
    cyan   |  c) color="${code}1;36m";;
    gray   | gr) color="${code}0;37m";;
    *) local text="$1"
  esac
  [ -z "$text" ] && local text="$color$2${code}0m"
  echo -e "$text"
}

cecho g "######################"
cecho g " time to update the framadate setup"
cecho g "######################"

COMMAND_BUILD="build:prod:demobliss"
git reset --hard
git pull origin master

#composer install
php bin/console doctrine:schema:update --force

cecho g "######################"
cecho g " update the funky frontend submodule "
cecho g "############################################"
cecho g " verification des besoins de l'application "
cecho g "############################################"
cecho g " yarn, git, php, node et une base de données"
cecho g "############################################${reset}"
# True if $1 is an executable in $PATH
# Works in both {ba,z}sh
function is_bin_in_path {
  if [[ -n $ZSH_VERSION ]]; then
    builtin whence -p "$1" &> /dev/null
  else  # bash:
    builtin type -P "$1" &> /dev/null
  fi
}

if [ ! type yarn &> /dev/null ]; then
  cecho red " ❌ la commande yarn est introuvable"
  exit 1
fi
cecho g " ✅ yarn"
if [ ! type node &> /dev/null ]; then
  cecho red " ❌ la commande node est introuvable"
  exit 1
fi
cecho g " ✅ node"
if [ ! type git &> /dev/null ]; then
  cecho g " ❌ la commande git est introuvable"
  exit 1
fi
cecho g " ✅ git"

if [ ! -f .env ]; then
  cecho g " ❌ fichier d'environnement de symfony /.env"
  exit 1
fi
cecho g " ✅ fichier d'environnement de symfony /.env ou /.env.local"

if [ ! -f bin/console ]; then
  cecho g " ❌ fichier console de symfony"
  exit 1
fi
cecho g " ✅ fichier console de symfony /bin/console "


if [ ! -d "funky-framadate-front" ]; then
  # initiate sub directory for funky front
  git submodule add -f https://framagit.org/framasoft/framadate/funky-framadate-front
  git submodule init
fi

git config --global diff.submodule log
git submodule update
cd funky-framadate-front
git clean -fd
git reset --hard && git checkout master && git pull
cd ..

cecho g "######################"
cecho g " check dependencies of the frontend with yarn "
cecho g "######################"
cd funky-framadate-front
cecho g "######################"
cecho g " debug info : version of programs"
cecho g " "
cecho b " git current branch "
git show-branch
cecho b " node version "
node -v
cecho b " yarn version "
yarn -v
cecho g " "
cecho b "##############################################"
cecho b " update of the funky part, clean of local repo"
cecho g " "
git reset --hard
git remote -v
git fetch
git pull
yarn
cecho g "######################"
cecho g " building the frontend, should take around 20 seconds "
cecho g "  "
cecho y " npm run $COMMAND_BUILD "
cecho g "  "
cecho y " start: $(date) "
cecho g "######################"
nvm use 16
npm run $COMMAND_BUILD

cecho g "######################"
cecho g " copying built files in the public folder of the symfony project "
cecho g "######################"
cd ../public
rm  ./*.js
rm  ./*.css
cd ../funky-framadate-front
cp -r dist/framadate/* ../public/
COUNT_FILES=$(ls -larth ../public |wc -l)
cd ..
cecho b " $COUNT_FILES fichiers de build copiés dans /public"
cecho g "##################################################################"
cecho b " renaming unieque name of JS chunks to common names for templates "
cd public

mv runtime* runtime.js
mv main* main.js
mv polyfills-es5* es5-polyfills.js
mv polyfills* other-polyfills.js
mv scripts* scripts.js
mv styles* styles.css

chown www-data:www-data . -R

cecho b " finished at ------- $(date) -------  "
cecho g "##################################################################"
cecho g " "
cecho b " done. you can now see your homepage updated "
cecho g " "
cecho g "##################################################################"
