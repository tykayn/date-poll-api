#!/bin/bash
# script d'update de framadate api, fait pour fonctionner avec le sous module git funky framadate


COMMAND_BUILD="build:prod:demobliss"
BRANCH="master"


# bash colors
cecho() {
  local code="\033["
  case "$1" in
    black  | bk) color="${code}0;30m";;
    red    |  r) color="${code}1;31m";;
    green  |  g) color="${code}1;32m";;
    yellow |  y) color="${code}1;33m";;
    blue   |  b) color="${code}1;34m";;
    purple |  p) color="${code}1;35m";;
    cyan   |  c) color="${code}1;36m";;
    gray   | gr) color="${code}0;37m";;
    *) local text="$1"
  esac
  [ -z "$text" ] && local text="$color$2${code}0m"
  echo -e "$text"
}

# vérifs prérequis

if [ ! type nvm &> /dev/null ]; then
  cecho red " ❌ la commande nvm est introuvable"
  exit 1
fi
cecho g " ✅ nvm"
if [ ! type node &> /dev/null ]; then
  cecho red " ❌ la commande node est introuvable"
  exit 1
fi
cecho g " ✅ node"
if [ ! type git &> /dev/null ]; then
  cecho red " ❌ la commande git est introuvable"
  exit 1
fi
cecho g " ✅ git"
if [ ! type yarn &> /dev/null ]; then
  cecho red " ❌ la commande yarn est introuvable"
  exit 1
fi
cecho g " ✅ yarn"

# use node version 16
nvm use 16

cd funky-framadate-front
git reset --hard && git checkout $BRANCH && git pull
cd ..

cecho g "######################"
cecho g " check dependencies of the frontend with yarn "
cecho g "######################"
cd funky-framadate-front
cecho g "######################"
cecho g " debug info : version of programs"
cecho g " "
cecho b " git current branch "
git show-branch
cecho b " node version "
node -v
cecho b " yarn version "
yarn -v
cecho g " "
cecho b "##############################################"
cecho b " update of the funky part, clean of local repo"
cecho g " "
git reset --hard
git remote -v
git fetch
git pull
yarn --pure-lockfile
cecho g "######################"
cecho g " building the frontend, should take around 20 seconds "
cecho g "  "
cecho y " yarn run $COMMAND_BUILD "
cecho g "  "
cecho y " start: $(date) "
cecho g "######################"
yarn run $COMMAND_BUILD


cecho g "######################"
cecho g " copying built files in the public folder of the symfony project "
cecho g "######################"
cd ../public
rm  ./*.js
rm  ./*.css
cd ../funky-framadate-front
cp -r dist/framadate/* ../public/
COUNT_FILES=$(ls -larth ../public |wc -l)
cd ..
cecho b " $COUNT_FILES fichiers de build copiés dans /public"
cecho g "##################################################################"
cecho b " renaming unique name of JS chunks to common names for templates "
cd public

mv runtime* runtime.js
mv main* main.js
mv polyfills-es5* es5-polyfills.js
mv polyfills* other-polyfills.js
mv scripts* scripts.js
mv styles* styles.css

chown tykayn:root . -R


cecho b " finished at ------- $(date) -------  "
cecho g "##################################################################"
cecho g " "
cecho b " done. you can now see your homepage updated "
cecho g " "
cecho g "##################################################################"




