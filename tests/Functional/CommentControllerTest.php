<?php

namespace App\Tests\Functional;

use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminControllerTest extends WebTestCase {
  use FixturesTrait;

  // Test getting all comments from one Poll
  public function testGetAllComments() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures',
        'App\DataFixtures\CommentFixtures',
    ));

    $client->request('GET', '/api/v1/poll/1/comments');

    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());
    $body = $response->getContent();
    $json = json_decode($body, true);
    $this->assertEquals(5, count($json['data']));

  }

  public function testNewComment() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures',
        'App\DataFixtures\CommentFixtures',
    ));

    $data = [
        'text' => "Mon nouveau commentaire de test !",
        'email' => "email@host.plop"
      ];

    $client->request('POST', '/api/v1/poll/1/comment', [
      'body' => json_encode($data)
      ], 
      [], 
      [
        'CONTENT_TYPE' => 'application/json',
        'HTTP_ACCEPT' => 'application/json',
      ],
      json_encode($data)
    );

      $response = $client->getResponse();
      $this->assertEquals(201, $response->getStatusCode());
      $body = $response->getContent();
      $json = json_decode($body, true);
      $this->assertEquals("email@host.plop", $json['data']['your_comment']['pseudo']);
  }

  public function testDeleteComments() {
    $client = static::createClient();

    $this->loadFixtures(array(
        'App\DataFixtures\AppPollFixtures',
        'App\DataFixtures\CommentFixtures',
    ));


    $client->request('DELETE', '/api/v1/poll/1/comments');
    $response = $client->getResponse();
    $this->assertEquals(200, $response->getStatusCode());

  }

}
